#include <stdio.h>
#include <unistd.h>         //Used for UART
#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART
#include <string.h>
#include <stdlib.h>

int configure_uart(){
	int uart0_filestream = -1;
	uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);      //Open in non blocking read/write mode
    if (uart0_filestream == -1)
    {
        printf("Erro - Não foi possível iniciar a UART.\n");
        close(uart0_filestream);
		exit(0);
    }
    else
    {
        printf("UART inicializada!\n");
    }
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;     //<Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);
	return uart0_filestream;
}

void read_uart(int uart0_filestream,int cmd){
	sleep(1);
	// Read up to 255 characters from the port if they are there
    unsigned char rx_buffer[256];
    char format[25];
	if(cmd == 0XA1 || cmd ==0XB1)
		strcpy(format, "%i Bytes lidos : %d\n");
	else if(cmd == 0XA2 ||cmd == 0xB2)
		strcpy(format ,"%i Bytes lidos : %f\n");
	else
		strcpy(format,"%i Bytes lidos : %s\n");

    int rx_length = read(uart0_filestream, (void*)rx_buffer, 255);      //Filestream, buffer to store in, number of bytes to read (max)
    if (rx_length < 0)
    {
        printf("Erro na leitura.\n"); //An error occured (will occur if there are no bytes)
    }
    else if (rx_length == 0)
    {
        printf("Nenhum dado disponível.\n"); //No data waiting
    }
    else
    {
        //Bytes received
        rx_buffer[rx_length] = '\0';
        printf(format, rx_length, rx_buffer);
    }
}

void write_uart(int uart0_filestream,char data[],int size){
	printf("Escrevendo caracteres na UART ...");
	int count = write(uart0_filestream, data, size);
	if (count < 0)
	{
		printf("UART TX error\n");
	}
	else
	{
		printf("escrito.\n");
	}
}

void request(int cmd,int uart0_filestream){
	char data[] = {cmd,0,3,3,1};
	write_uart(uart0_filestream,data,5);
}

void send(int cmd,int uart0_filestream){
	char inputB3[100];
	char data[6],data_s[107];
	float inputB2;
	int inputB1,len;
	data[0] = cmd;
	data[2]= 0;
	data[3]=3;
	data[4] = 3;
	data[5]= 1;

	switch(cmd){
		case 0XB1:
			printf("digite o inteiro que quer enviar \n");
			scanf("%d", &inputB1);
			data[1]=inputB1;
			write_uart(uart0_filestream,data,5 + sizeof(int));
			break;
		case 0XB2:
			printf("digite o float que quer enviar \n");
			scanf("%f", &inputB2);
			data[1]=inputB2;
			write_uart(uart0_filestream,data,5 + sizeof(float));
			break;
		case 0XB3:
			printf("digite a string que quer enviar \n");
			scanf("%s", inputB3);
			data_s[0] = cmd;
			data_s[1] = strlen(inputB3);
			data_s[2] = '\0';
			strcat(data_s,inputB3);
			len = strlen(data_s);
			memset(&data_s[len],0,1);
			memset(&data_s[++len],3,1);
			memset(&data_s[++len],3,1);
			memset(&data_s[++len],1,1);
			write_uart(uart0_filestream,data_s,len + 1);
			break;
	}
}
int main(int argc, const char * argv[]) {

	int uart0_filestream = configure_uart();

	int cmd = 0;
	printf("digite o comando em hexadecimal \n");
	scanf("%x",&cmd);
	if(cmd > 0XA0  && cmd < 0XA4)
		request(cmd,uart0_filestream);
	else if(cmd > 0XB0 && cmd < 0XB4)
		send(cmd,uart0_filestream);
	else{
		printf("comando invalido !!! \n");
		close(uart0_filestream);
		exit(0);
	}

	read_uart(uart0_filestream,cmd);

    close(uart0_filestream);
    return 0;
}
